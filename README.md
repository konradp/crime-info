# php-nginx-example
An example PHP app served by Nginx.  
It returns a list of crimes and outcomes for a given postcode and date.

# Prod
http://php-nginx-example.tk:8000/health  
http://php-nginx-example.tk:8000/crimes/SL1%204PN/2018-03  

# Test
Test run on local machine with
```
cd src
php -S localhost:8000
```
and access on http://localhost:8000  

Example  
http://localhost:8000/crimes/SL1%204PN/2018-03  
should return  
```
[
  {
    "crime": {
      "date": "2018-03",
      "type": "violent-crime"
    },
    "outcome": {
      "date": "2018-03",
      "type": "Investigation complete; no suspect identified"
    }
  }
]
```

# Requirements: prod deployment

- php-fpm
- nginx
