Name:           %{?appname}%{!?appname:app}
BuildArch:      noarch
Version:        %{?version}%{!?version:0.0}
Release:        1%{?dist}
Requires:       php-fpm nginx
Summary:        Some summary
License:        GPLv3+
URL:            http://example.com/%{name}
Source0:        http://example.com/%{name}-%{version}.tar.gz

%define targetdir /usr/share/nginx/html
# Do not build debug package
%define debug_package %{nil}

%description

%prep
%setup -n %{name}
ls -lh

%build
echo Build stage
ls -lh

%install
echo Install stage
ls -lh $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{targetdir}
ls -lh
cp src/* $RPM_BUILD_ROOT%{targetdir}

%files
%{targetdir}/index.php

%changelog
