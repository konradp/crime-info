<?php
/***
  Given a postcode and date, return a list of crimes and their outcomes.
  Uses a basic switch-based to handle HTTP GET requests.
  Example:
  Input: HTTP GET request to
         http://localhost/crimes/POSTCODE/DATE
  Output: An array of crime info objects:
    - crime: date, type
    - outcome: date, type
***/

// Config
define('POSTCODE_URI', 'http://postcodes.io/postcodes');
define('POLICE_URI', 'https://data.police.uk/api/crimes-at-location');

// Init
$method = $_SERVER['REQUEST_METHOD'];
$uri= $_SERVER['REQUEST_URI'];

// MAIN: Handle request
switch($method) {
  case 'GET':
    // Url to array, check number of args
    $args = explode('/', parse_url($uri, PHP_URL_PATH)); // Path
    if(sizeof($args) != 2 && sizeof($args) != 4) ret(404);
    // Router
    switch($args[1]) {
      case 'health':
        // /health
        ret(200, '{ "status": "OK" }');
        break;
      case 'crimes':
        // /crimes/POSTCODE/DATE
        if(sizeof($args) != 4) ret(404);
        $postcode = urldecode($args[2]);
        $date = urldecode($args[3]);
        getCrimes($postcode, $date);
        ret(404, "Missing parameters"); // If we got here, error
        break;
      default: ret(404);
    }
    break; // GET
  default: ret();
}

/* API: External */
function getCrimes($postcode, $date) {
  try {
    $geo = getGeo($postcode);
    $ret = getCrimesFromPolice($date, $geo['latitude'], $geo['longitude']);
    if(!empty($ret))
      ret(200, json_encode($ret));
    else
      ret(500, 'Unable to get crimes.');
  } catch(Exception $e) {
    ret(500, $e->getMessage());
  }
  ret(500, 'Uncaught exception');
}

function getGeo($postcode) {
  // Get geolocation for given postcode
  // OUTPUT: Array
  //   - latitude
  //   - longitude
  if(!isset($postcode)) die('No postcode set');
  $geo = file_get_contents(POSTCODE_URI."/$postcode");
  $geo = json_decode($geo, true);
  if(empty($geo)) throw New Exception('Could not get geolocation');
  if(isset($geo['result']['latitude'])
      && isset($geo['result']['longitude'])
  ) {
    return Array(
      'latitude' => $geo['result']['latitude'],
      'longitude' => $geo['result']['longitude']
    );
  } else {
    throw new Exception('Could not get geolocation');
  }
}

function getCrimesFromPolice($date, $latitude, $longitude) {
  // Get crimes from given geolocation
  // OUTPUT: Array of crimes
  // - crime: date,type
  // - outcome: date, type
  if(!isset($latitude) || !isset($longitude))
    die('No postcode set');
  $uri = POLICE_URI."?date=$date&lat=$latitude&lng=$longitude";
  $crimes = file_get_contents($uri);
  $crimes = json_decode($crimes, true);
  if(empty($crimes)) throw New Exception('Could not get geolocation');
  $ret = [];
  foreach($crimes as $c) {
    if(
        isset($c['category'])
        && isset($c['outcome_status']['date'])
        && isset($c['outcome_status']['category'])
    ) {
      // Extract data
      array_push($ret, Array(
        'crime' => Array(
          'date' => $date,
          'type' => $c['category']
        ),
        'outcome' => Array(
          'date' => $c['outcome_status']['date'],
          'type' => $c['outcome_status']['category']
        )
      )); // Array push
    } else {
      throw new Exception('Could not get geolocation');
    }
  }
  return $ret;
}

// HTTP return codes
function ret($code, $text = NULL) {
  header('Content-Type: application/json');
  if(isset($text)) echo $text;
  http_response_code(200);
  die();
}
/* /Helpers */
?>
